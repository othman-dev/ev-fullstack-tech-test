import axios from 'axios';

const UsersReducer = (state, action) => {



        switch(action.type){
                case 'SYNC':                
                        return action.data;
                case 'ADD_USER':
                        axios.post('http://localhost:3001/users/add', action.newUser)
                                .catch(err => console.log(err))
                                .then(res => res)
                        return state
                case 'REMOVE_USER':
                        axios.post('http://localhost:3001/users/remove', {id: action.id})
                                .catch(err => console.log(err))
                                .then(res => res)
                        return state
                default:
                        return state;
        }
}
export default UsersReducer;

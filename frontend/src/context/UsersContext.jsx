import React, {createContext, useReducer, useState, useEffect} from 'react';
import UsersReducer from './UsersReducer';
import axios from 'axios';

export const UsersContext = createContext();

const UsersContextProvider = (props) => {

	const [usersList, dispatch] = useReducer(UsersReducer, []);

	// State to skip first syncing and to reload when needed
	const [skiping, setSkiping] = useState(true)

	// Sync the data on start and every action
	useEffect(async () => {
		skiping && await axios.get('http://localhost:3001/users/get')
			.catch(err => console.log(err))
            .then(res => {
				dispatch({ type: 'SYNC', data: res.data })
			})
		setSkiping(false)
	},[skiping])

	return (
		<UsersContext.Provider value={{usersList, dispatch, setSkiping}}>
		{props.children}
		</UsersContext.Provider>
	);
};
export default UsersContextProvider
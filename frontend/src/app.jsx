import React from "react";
import './app.scss';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import UsersContextProvider from './context/UsersContext';
import Home from './components/Home';
import UsersManager from './components/UsersManager';
import ErrorPage from './components/ErrorPage';


const App = () => {

  // redirect since home page is empty
  
  return (
    <div className='mainContainer'>
      <h1 className='mainTitle'>EVPro Full-stack Test</h1>
      <UsersContextProvider>
      <Router>
        <Routes>
          <Route path='/' element={<Navigate to='/users-manager'/>}/>
          <Route path='/users-manager' element={<UsersManager/>}/>
          <Route path='*' element={<ErrorPage/>}/>
        </Routes>
      </Router>
      </UsersContextProvider>
    </div>
  );
};

export default App;

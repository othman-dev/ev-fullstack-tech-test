import React from 'react';
import './ErrorPage.scss'

const ErrorPage = () => {

    return (
        <div>
            <h1 className='errorMessage'>Error 404, Page not found !!!</h1>
        </div>
    )
}

export default ErrorPage;
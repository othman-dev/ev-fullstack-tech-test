import React, { useState, useContext } from "react";
import './AddUserWindow.scss';
import { UsersContext } from '../../../context/UsersContext';
import { Navigate } from 'react-router-dom';

const AddUserWindow = (props) => {

    // State creation for new user and dispatch from context
    const [newUser, setNewUser] = useState({
        name : '',
        company: '',
        email: ''
    });
    const { dispatch, setSkiping } = useContext(UsersContext)

    // Form handlers
    function handleChange(e) {
        e.preventDefault()
        setNewUser({...newUser, [e.target.name]:e.target.value})
    };

    function handleSubmit(e) {
        e.preventDefault()
        dispatch({ type:'ADD_USER', newUser })
        setTimeout(() => {
            props.toggle();
            setSkiping(true)
        }, 500)
    };

    return (
        <div className='formContainer'>
            <form className='userForm' onSubmit={handleSubmit}>
                <h2 className='formHeader'>Enter User details</h2>
                <div className='formExit' onClick={() => props.toggle()}> X </div>
                <input className='formInput' type='text' name='name' placeholder='Name' onChange={handleChange} required/>
                <input className='formInput' type='text' name='company' placeholder='Company' onChange={handleChange} required/>
                <input className='formInput' type='email' name='email' placeholder='Address@Email.com' onChange={handleChange} required/>
                <input className='formSubmit' type='submit' value='Submit User' onClick={() => handleSubmit}/>
            </form>
        </div>
    );
};

export default AddUserWindow;

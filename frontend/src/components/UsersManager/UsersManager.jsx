import React, { useContext, useState } from "react";
import './UsersManager.scss'
import { UsersContext } from '../../context/UsersContext';
import AddUserWindow from "./AddUserWindow"


const UsersManager = () => {

    // State to toggle new User pop-up, research function state and data from context
    const [addUser, toggleAddUser] = useState(false)
    const [research, setResearch] = useState('')
    const { usersList, dispatch, setSkiping } = useContext(UsersContext)

    // Research handler
    function handleChange(e) {
        e.preventDefault()
        setResearch(e.target.value.toLowerCase())
    }

    // User display structure
    const User = (user) =>
        <div className='userFormat' key={user._id}>
            <p className='userInfo'><span>Name:</span> {user.name}</p>
            <p className='userInfo'><span>Company:</span> {user.company}</p>
            <p className='userInfo'><span>Email:</span> {user.email}</p>
            <p className='userInfo'><span>Date of creation:</span> {user.creationDate}</p>
            <div className='userInfo delete' onClick={() => {
                dispatch({type:'REMOVE_USER', id:user._id}); setTimeout(() => setSkiping(true), 500)}}>X</div>
        </div>
        
    // Simple research function, easy settings with filter 
    const filterList = usersList.filter(user => {
        if(user.name.toLowerCase().includes(research) | 
        user.email.toLowerCase().includes(research) | 
        user.company.toLowerCase().includes(research) ){
           return User(user)
        } 
    })

    return (

        <div className='usersManagerContainer'>
            <h2>Users Manager</h2>
            <h5>{`${usersList.length} users registered.`}</h5>
            <button  className='addButton' onClick={() => toggleAddUser(!addUser)}>Add a user</button>  
            <input className='searchBar' placeholder='Type here to start searching' type='text' onChange={handleChange}></input>
            {addUser && <AddUserWindow toggle={() => toggleAddUser(!addUser)}/> }
            <div className='userContainer'>
                {research ?
                    filterList.map(user => User(user)) :
                    usersList.map(user => User(user))
                }
            </div>
        </div>
    );
};

export default UsersManager;
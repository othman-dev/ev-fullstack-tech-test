const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const DbManager = require('./utils/DbManager');
const UsersRoutes = require('./routes/Users')

const app = express();

// use CORS and parse from body
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connect to Db
DbManager();

// Users Routes
app.use('/users', UsersRoutes)

// Server Listen
app.listen(3001, () => {
  console.log('Server started on port 3001');
});

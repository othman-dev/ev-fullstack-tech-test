const express = require('express');
const UsersController = require('../controllers/UsersController');

const router = express.Router();

router.get('/get', UsersController.getUsers);
router.post('/add',UsersController.addUser);
router.post('/remove',UsersController.removeUser);

module.exports = router;
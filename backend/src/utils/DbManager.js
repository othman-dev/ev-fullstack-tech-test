require('dotenv').config()
const mongoose = require('mongoose')

const DbManager = async () => {
    
    const dbURI = process.env.MONGODB_URI

    await mongoose.connect(dbURI)
        .then( result => console.log('connected to db...' ))
        .catch(err => console.log(err))

}

module.exports = DbManager;
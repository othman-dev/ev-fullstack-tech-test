const GetDate = () => {

    const d = new Date()
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    const date =  `${d.getDate().toString()} ${monthNames[d.getMonth()]} ${d.getFullYear().toString()}`

    return date
}

module.exports = GetDate;
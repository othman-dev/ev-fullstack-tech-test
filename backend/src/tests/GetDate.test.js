const GetDate = require('../utils/GetDate')

describe('Date format is "dd mmm yyyy"', () => {

    test('Date length is 11', () => {
        expect(GetDate().length()).toBe(11)
    })
    
    test('Day is a number between 01 and 31', () => {
        const day = parseInt(GetDate().slice(0,1))
        expect(day).toBeMoreThan(0)
        expect(day).toBeLessThan(32)
    })

    test('Year is a number between 2000 and 3000', () => {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        const month = GetDate().slice(3,5)
        expect(monthNames).toContain(month)
    })

    test('Year is a number between 2000 and 3000', () => {
        const year = parseInt(GetDate().slice(7,10))
        expect(year).toBeMoreThan(2000)
        expect(year).toBeLessThan(3000)
    })    
})
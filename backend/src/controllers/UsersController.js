const User = require('../models/user');
const GetDate = require('../utils/GetDate');


const getUsers = (req, res, next) => {
    User.find()
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      console.log(err);
    });
}

const addUser = (req, res, next) => {
    const user = new User({
        ...req.body, 
        creationDate: GetDate()
      })
      
      user.save()
        .then( result => {
          res.send(result)
        })
        .catch(err => {
          console.log(err)
        })
}

const removeUser = (req, res, next) => {
      
      User.deleteOne({_id:req.body.id})
        .then( result => {
          res.send(result)
        })
        .catch(err => {
          console.log(err)
        })
}


module.exports = {
  getUsers,
  addUser,
  removeUser
}